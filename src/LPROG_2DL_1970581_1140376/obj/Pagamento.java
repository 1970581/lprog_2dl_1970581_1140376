/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LPROG_2DL_1970581_1140376.obj;

import java.util.Locale;

/**
 *
 * @author Hugo
 */
public class Pagamento {
    //<pagamento tipopagamento="dinheiro" datapagamento="2016-05-25" euros="500.00"/>
    
    String pagamento;
    String data;
    float dinheiro;
    
    public Pagamento(int x, String data, float dinheiro){
        this.data = data;
        this.dinheiro = dinheiro;
        this.pagamento = "dinheiro";
        if (x == 2) pagamento = "cheque";
        if (x == 3) pagamento = "cartão";
    }

    @Override
    public String toString() {
        String euro = String.format(Locale.US , "%.2f",(float)dinheiro);
        return "              <pagamento tipopagamento=\""+pagamento+"\" datapagamento=\""+ data+"\" euros=\""+euro+"\" moeda=\"€\"/>\n";
    }
    
    
}
