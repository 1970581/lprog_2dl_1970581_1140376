/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LPROG_2DL_1970581_1140376.obj;

/**
 *
 * @author Hugo
 */
public class Veiculo {
    
    /*
    <veiculos>
        <veiculo matricula="AB-12-CD">
            <tipo>carro</tipo>
            <combustivel>gasolina</combustivel>
            <capacidade>2</capacidade>
            <comentario> pisca partido </comentario>
        </veiculo>        
    </veiculos>
    */
    
    String matricula;
    String tipo;
    String combustivel;
    int capacidade;
    String comentario;

    public Veiculo(String matricula, String tipo, String combustivel, int capacidade, String comentario) {
        this.matricula = matricula;
        this.tipo = tipo;
        this.combustivel = combustivel;
        this.capacidade = capacidade;
        this.comentario = comentario;
    }

    @Override
    public String toString() {
        String linha1 = "  <veiculo matricula=\"" + this.matricula +"\">\n";
        String linha2 = "     <tipo>" + this.tipo+"</tipo>\n";
        String linha3 = "     <combustivel>" + this.combustivel+"</combustivel>\n";
        String linha4 = "     <capacidade>" + this.capacidade+"</capacidade>\n";
        String linha5 = "  </veiculo>";
        if (comentario != null) linha5 = "      <comentario>"+this.comentario+"</comentario>\n</veiculo>";
        
        String out = linha1 + linha2 + linha3 + linha4 + linha5;
            return out;
    }
    
    
    
    
    
}
