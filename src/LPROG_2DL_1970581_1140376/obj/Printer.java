/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LPROG_2DL_1970581_1140376.obj;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author Hugo
 */
public class Printer {
    
    
    private List<Veiculo> listaVeiculos;
    private List<Cliente> listaClientes;
    private List<Aluger> listaAluger;
    
    
    
    public Printer(List<Veiculo> listaVeiculos, List<Cliente> listaClientes, List<Aluger> listaAluger) {
        this.listaVeiculos = listaVeiculos;
        this.listaClientes = listaClientes;
        this.listaAluger = listaAluger;
    }
    
    
    
    
    
    
    public void print(){
        try {
            PrintWriter writer = new PrintWriter("src/LPROG_2DL_1970581_1140376/db.xml", "UTF-8");
            writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            writer.println("<db xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"dbSchema.xsd\">");
            writer.println("");
            writer.println("<veiculos>");
            
            for(Veiculo v : this.listaVeiculos) writer.println(v);
            
            writer.println("</veiculos>");
            writer.println("\n");
            writer.println("<clientes>");
            
            for(Cliente c : this.listaClientes) writer.println(c);
            
            writer.println("</clientes>");
            writer.println("\n");
            writer.println("<alugeres>");
            
            for (Aluger al : this.listaAluger) writer.println(al);
            
            writer.println("</alugeres>");
            writer.println("</db>");
            
            
            writer.close();
        } catch (IOException e) {
                System.out.println("ERRO: "+ e);// do something
        }
    }
}
