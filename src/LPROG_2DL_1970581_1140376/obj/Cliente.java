/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LPROG_2DL_1970581_1140376.obj;

/**
 *
 * @author Hugo
 */
public class Cliente {
    
    
    /*
    <cliente idcliente="1">
            <firstname>Joao</firstname>
            <lastname>Lopes</lastname>
            <cartao>
                <cc>123456789 aa1</cc>
            </cartao>
            <carta>P-1234567</carta>
            <nascimento>1900-01-01</nascimento>
            <foto>12345678.jpg</foto>
            <comentario> fuma </comentario>
        </cliente>
    */
    
    int idcliente;
    String firstname;
    String lastname;
    String ccbi;
    boolean cc;
    String carta;
    String dataNascimento;
    String foto;
    String comentario;
    Character sexo;

    public Cliente(int idcliente, String firstname, String lastname, Character sexo,boolean cc,String ccbi, String carta, String dataNascimento, String foto, String comentario) {
        this.idcliente = idcliente;
        this.firstname = firstname;
        this.lastname = lastname;
        this.cc = cc;
        this.ccbi = ccbi;
        this.carta = carta;
        this.dataNascimento = dataNascimento;
        this.foto = foto;
        this.comentario = comentario;
        this.sexo = sexo;
    }
    
    @Override
    public String toString() {
        String strSex = "";
        if (this.sexo != null) {
            if (sexo.equals('f')) strSex = " sex=\"f\" ";
            if (sexo.equals('m')) strSex = " sex=\"m\" ";
        }
        String linha1 = " <cliente idcliente=\"" + this.idcliente + "\" "+ strSex +">\n";
        String linha2 = "    <firstname>" + this.firstname+"</firstname>\n";
        String linha3 = "    <lastname>" + this.lastname+"</lastname>\n";
        String linha4 = "    <cartao> <cc>" + this.ccbi+"</cc> </cartao>\n";
        if (!cc) linha4 = "  <cartao> <bi>" + this.ccbi+"</bi> </cartao>\n";
        String linha5 = "    <carta>" + this.carta +"</carta>\n";
        String linha6 = "    <nascimento>" + this.dataNascimento +"</nascimento>\n";
        String linha7 = "    <foto>" + this.foto +"</foto>\n";
        String linha8 = " </cliente>";
        if (comentario != null) linha8 = "      <comentario>"+this.comentario+"</comentario>\n</cliente>";
        
        String out = linha1 + linha2 + linha3 + linha4 + linha5 + linha6 + linha7 + linha8;
            return out;
    }
    
    
}
