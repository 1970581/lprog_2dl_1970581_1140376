/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LPROG_2DL_1970581_1140376.obj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Hugo
 */
public class GeradorAluger {
    
    private List<Veiculo> listaVeiculos;
    private List<Cliente> listaClientes;
    private List<Aluger> listaAluger;

    public GeradorAluger(List<Veiculo> listaVeiculos, List<Cliente> listaClientes, List<Aluger> listaAluger) {
        this.listaVeiculos = listaVeiculos;
        this.listaClientes = listaClientes;
        this.listaAluger = listaAluger;
    }
    
    public List<Aluger> gerarLista(){
        int numeroAlugeres = 1000;
        
        Calendar data = GregorianCalendar.getInstance();
        
        int custoAluger = 0;
        int nPagamentos = 1;
        
        for (int i = 1; i<= numeroAlugeres; i++){
            
            Calendar diaAluger = (Calendar) data.clone();
            diaAluger.add(Calendar.DATE, i);
            this.listaAluger.add(gerarAluger(i, diaAluger));
        }
        
        
        
        return listaAluger;
    }
    
    
    private Aluger gerarAluger(int idAluger, Calendar dia){
        int maxV = this.listaVeiculos.size(); //20
        int maxC = this.listaClientes.size(); //20
        
        int v = (int) (Math.random() * maxV);
        int c = (int) (Math.random() * maxC);
        
        String matricula = this.listaVeiculos.get(v).matricula;
        int idcliente = this.listaClientes.get(c).idcliente;
        
        float custoAluger = rP();
        int nPagamentos = rNp();
        int dias = (int) ((Math.random() * 5) +1);
        
        List<Pagamento> listaPagamentos = new ArrayList();
        
        String comentario = rComentario();
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dataInicio = sdf.format( dia.getTime() );
        
        for(int i = 0; i<nPagamentos; i++){
            
            int typoPagamento = (int) ((Math.random() *10) +1);
            float euros = custoAluger / nPagamentos;
            Calendar diaPag = (Calendar) dia.clone();
            //Calendar dataPag = (Calendar) dataInicio.clone();
            diaPag.add(Calendar.DATE, i);
            
            listaPagamentos.add(new Pagamento(typoPagamento, sdf.format(diaPag.getTime()) , euros));
        }
        
        
        Aluger aluger = new Aluger(idAluger, dataInicio , dias, custoAluger, idcliente, matricula, comentario, listaPagamentos);
        return aluger;        
    }
    
    
    
    // Numero random de 1 a 4.
    private int rNp(){
        int x = (int) ((Math.random()*20) +1);
        if (x > 4) return 1;
        if (x == 3) return 2;
        if (x == 2) return 3;
        return 4;
    }
    
    private float rP(){
        return (float) (Math.random() * 100)+1 ;
    }
    
    private String rComentario(){
        ArrayList<String> lista = new ArrayList();
        
        lista.add(" Voltou riscado.");
        lista.add(" Cheio de lixo");
        lista.add(" o cliente queixou-se do consumo");
        lista.add(" o tanque voltou vazio");
        lista.add(" existiam ratos no carro");
        lista.add(" cliente limpou o carro");
        lista.add(" nada a asinalar");
        lista.add(null);
        lista.add(" carro vomitado");
        lista.add(" cliente deixou a carteira no carro");
        lista.add(" tabaco encontrado no banco de tras");
        lista.add(" direcao desalinhada");
        lista.add(" entregou tarde o carro");
        lista.add(" migalhas por todo o carro");
        lista.add(" mala sem colete");
        lista.add(" a mala continha algo esquecido");
        lista.add(" cliente indica carro perde oleo");
        lista.add(null);
        
        
        int size = lista.size();
        int pos = (int) (Math.random() * size) ;
        return  lista.get(pos) ;
    }
}
