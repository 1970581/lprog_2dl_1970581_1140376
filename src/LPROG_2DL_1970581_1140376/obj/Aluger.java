/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LPROG_2DL_1970581_1140376.obj;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author Hugo
 */
public class Aluger {
    /*
    <aluger idaluger="1">
            <inicio>2016-05-25</inicio>
            <duracao>5</duracao>
            <preco>100.00</preco>
            <idcliente>1</idcliente>
            <idcarro>AB-12-CD</idcarro>
            <comentario> pediu desconto </comentario>
            <pagamento tipopagamento="dinheiro" datapagamento="2016-05-25" euros="500.00"/>
        </aluger>
    */
    
    int idAluger;
    String inicio;
    int duracao;
    float preco;
    int idCliente;
    String idCarro;
    String comentario;
    List<Pagamento> listaPagamentos;

    public Aluger(int idAluger, String inicio, int duracao, float preco, int idCliente, String idCarro, String comentario, List<Pagamento> listaPagamentos) {
        this.idAluger = idAluger;
        this.inicio = inicio;
        this.duracao = duracao;
        this.preco = preco;
        this.idCliente = idCliente;
        this.idCarro = idCarro;
        this.comentario = comentario;
        this.listaPagamentos = new ArrayList();
        if(listaPagamentos != null){
            for (Pagamento p : listaPagamentos){
                this.listaPagamentos.add(p);
            }
        }
    }
    
    
    public void addPagamento(Pagamento p){
        this.listaPagamentos.add(p);
    }

    @Override
    public String toString() {
        String linha1 = " <aluger idaluger=\"" + this.idAluger +"\">\n";
        String linha2 = "     <inicio>" + this.inicio+"</inicio>\n";
        String linhaa = "     <duracao>" + this.duracao + "</duracao>";
        String linha3 = "     <preco>" + String.format(Locale.US,"%.2f",(float) this.preco) +"</preco>\n";
        //linha3 = "<preco>" + this.preco +"</preco>\n";
        String linha4 = "     <idcliente>"+ this.idCliente+"</idcliente>\n";
        String linha5 = "     <idcarro>"+ this.idCarro+"</idcarro>\n";
        
        String linha6 = "";
        if (comentario != null) linha6 = "      <comentario>"+this.comentario+"</comentario>\n";
        String linha7 = "";
        for(Pagamento p : listaPagamentos){
            linha7 = linha7.concat(p.toString());
        }
        String linha8 = " </aluger>";
        
        String out = linha1 + linha2 + linhaa + linha3 + linha4 + linha5 + linha6 + linha7 + linha8;
            return out;
    }
    
    /*
    <aluger idaluger="1">
            <inicio>2016-05-25</inicio>
            <duracao>5</duracao>
            <preco>100.00</preco>
            <idcliente>1</idcliente>
            <idcarro>AB-12-CD</idcarro>
            <comentario> pediu desconto </comentario>
            <pagamento tipopagamento="dinheiro" datapagamento="2016-05-25" euros="500.00"/>
        </aluger>
    */
    
}
