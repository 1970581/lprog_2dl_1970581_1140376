<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : db.xsl
    Created on : May 11, 2017, 3:38 PM
    Author     : Hugo
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    
    
    <xsl:template match="/db">
       <html>            
            <body>  
                
                <xsl:apply-templates select="veiculos"/>
                <xsl:apply-templates select="clientes"/>
                <xsl:apply-templates select="alugeres"/>
            </body> 
       </html>
    </xsl:template>
    
    <xsl:template match="veiculos">
        <h2> Listagem de Veiculos </h2>
                  <table border="1">
                      <colgroup>
                      <col align="right"/>
                      <col align="center"/>
                      <col align="center"/>
                      <col align="center"/>
                      <col align="center"/>
                      <col align="center"/>
                      </colgroup>
                    <tr>
                        <th align="center"> # </th>
                        <th align="center"> Matricula </th>
                        <th align="center"> Lugares </th>
                        <th align="center"> Classe </th>
                        <th align="center"> Combustivel </th>
                        <th align="center"> Comentario </th>
                                                
                    </tr>  
                    
                    <!--<xsl:apply-templates /> -->
                    <!--<xsl:sort select="@matricula"/>-->
                    <xsl:apply-templates select="veiculo" >
                        <xsl:sort select="@matricula"/>
                    </xsl:apply-templates>
                   </table>
                  <br/>
                  
    </xsl:template> 
                    
                    
                    
    <xsl:template match="veiculo">
                    
                        
                        <tr>
                            <td align="center"> <xsl:value-of select="position()"/> </td>
                            <td align="center"> <xsl:value-of select="@matricula"/> </td>
                            <td align="center"> <xsl:value-of select="capacidade"/> </td>
                            <td align="center"> <xsl:value-of select="tipo"/> </td>
                            <td align="center"> <xsl:value-of select="combustivel"/> </td>
                            <td align="center">
                                <xsl:if test="./comentario"> 
                                    <xsl:value-of select="comentario"/> 
                                </xsl:if> 
                            </td>
                            
                        </tr>
    </xsl:template>
    
    
    <xsl:template match="clientes">
        <h2> Listagem de Clientes</h2>
                  por ordem alfabetica.
                  <table border="1">
                    <tr>
                        <th align="center"> # </th>
                        <th align="center"> nº cliente </th>
                        <th align="center"> Nome </th>
                        <th align="center"> Apelido </th>
                        <th align="center"> m/f </th>
                        <th align="center"> nascido a </th>
                        <th align="center"> foto </th>
                        <th align="center"> ID </th>
                        <th align="center"> ºn </th>
                        <th align="center"> comentario </th>
                        
                                                
                    </tr>  
                    
                    <xsl:apply-templates select="cliente" >
                        <xsl:sort select="firstname"/>
                        <xsl:sort select="lastname"/>
                    </xsl:apply-templates>
                   </table>
                  <br/>
                    
    </xsl:template>
    
    <xsl:template match="cliente">
        <tr>
                            <td align="center"> <xsl:value-of select="position()"/> </td>
                            <td align="center"> <xsl:value-of select="@idcliente"/> </td>
                            <td align="center"> <xsl:value-of select="firstname"/> </td>
                            <td align="center"> <xsl:value-of select="lastname"/> </td>
                            <td align="center">
                                <xsl:if test="./@sex"> <xsl:value-of select="@sex"/> </xsl:if>
                            </td>
                            
                            
                            <td align="center"> <xsl:value-of select="nascimento"/> </td>
                            
                            <td align="center">
                                <xsl:variable name="v1" select="foto"/>
                                <img src="{$v1}" alt="X" title="{$v1}" width="30" height="30"/>
                            </td>
                            <td align="center">
                                <xsl:if test="./cartao/cc"> cc </xsl:if>
                                <xsl:if test="./cartao/bi"> bi </xsl:if>
                            </td>
                            <td align="center">
                                <xsl:if test="./cartao/cc">  <xsl:value-of select="cartao/cc"/>  </xsl:if>
                                <xsl:if test="./cartao/bi">  <xsl:value-of select="cartao/bi"/>  </xsl:if>
                            </td>
                            <td align="center">
                                <xsl:if test="./comentario"> 
                                    <xsl:value-of select="comentario"/> 
                                </xsl:if> 
                            </td>
                            
                        </tr>
    </xsl:template>
    <xsl:template match="alugeres">
        <h2> Listagem de Alugeres</h2>
                  por data.
                  
                  <table border ="2">
                    <tr>
                        <th align="center"> # </th>
                        <th align="center"> dia </th>
                        <th align="center"> Cliente </th>
                        <th align="center"> Carro </th>
                        <th align="center"> Duracao </th>                        
                        <th align="center"> Preço </th>
                        <th align="center"> Preço dia </th>
                        <th align="center"> Comentario </th>
                        <th align="center"> Pagamentos </th>
                                                
                    </tr>  
                    
                    <xsl:apply-templates select="aluger" >
                         <xsl:sort select="inicio"/>
                    </xsl:apply-templates>
                    
                  </table>
    </xsl:template>
    
    <xsl:template match="aluger">
        <tr>
                            <td align="center"> <xsl:value-of select="position()"/> </td>
                            <td align="center"> <xsl:value-of select="inicio"/> </td>
                            <td align="center"> <xsl:value-of select="idcliente"/> </td>
                            <td align="center"> <xsl:value-of select="idcarro"/> </td>
                            <td align="center"> <xsl:value-of select="duracao"/> </td>
                            <td align="center"> <xsl:value-of select="preco"/> € </td>
                            <!-- preco dia = preco / numero dias. , formatamos o numero para 2 casas decimais. format-number($pi,'#.####')-->
                            <td align="center"> <xsl:value-of select="format-number(./preco div ./duracao,'#.##')"/> € </td>
                            <td align="center">
                                <xsl:if test="./comentario"> <xsl:value-of select="comentario"/> </xsl:if> 
                                <xsl:if test="not(./comentario)"> - -- - </xsl:if> 
                            </td>
                            <td align="center">
                                <ul>
                                    <xsl:for-each select="./pagamento" >
                                        <li>
                                            Pagou <xsl:value-of select="@euros"/>&#160;<!--espaco--><xsl:value-of select="@moeda"/> em 
                                            <xsl:value-of select="@datapagamento"/> por
                                            <xsl:value-of select="@tipopagamento"/> .
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </td>
                        </tr>
    </xsl:template>
    
    
    <!--
    /()==============================================================\
     ||
     ~     _____________________________
      ()==(                            (@==()
          '____________________________'|
           |  O velho Template antes    |
           |  da conversão para multi   |
           |  Templates, em vez de      |
           |  for-each.                 |
           |  RIP for-each : 2016-2017  |
         __)____________________________|
    ()==(                              (@==()
         '~~~~~~~~~~~~~~~~~~~~~~~~~~~~-'    
    -->
    <xsl:template match="/lixoOldNaoUsar">
       <html>            
            <body>  
                
                <xsl:apply-templates select="veiculos"/>
                <xsl:apply-templates select="clientes"/>
                <xsl:apply-templates select="alugeres"/>
                
                
                 
                  <h2> Listagem de Veiculos</h2>
                  <table border="1">
                      <colgroup>
                      <col align="right"/>
                      <col align="center"/>
                      <col align="center"/>
                      <col align="center"/>
                      <col align="center"/>
                      <col align="center"/>
                      </colgroup>
                    <tr>
                        <th align="center"> # </th>
                        <th align="center"> Matricula </th>
                        <th align="center"> Lugares </th>
                        <th align="center"> Classe </th>
                        <th align="center"> Combustivel </th>
                        <th align="center"> Comentario </th>
                                                
                    </tr>  
                    <xsl:for-each select="/db/veiculos/veiculo" >
                        <xsl:sort select="@matricula"/>
                        <tr>
                            <td align="center"> <xsl:value-of select="position()"/> </td>
                            <td align="center"> <xsl:value-of select="@matricula"/> </td>
                            <td align="center"> <xsl:value-of select="capacidade"/> </td>
                            <td align="center"> <xsl:value-of select="tipo"/> </td>
                            <td align="center"> <xsl:value-of select="combustivel"/> </td>
                            <td align="center">
                                <xsl:if test="./comentario"> 
                                    <xsl:value-of select="comentario"/> 
                                </xsl:if> 
                            </td>
                            
                        </tr>
                    </xsl:for-each>
                  </table>
                  <br/>
                  
                  <!-- ================================================= -->
                  
                  
                  <h2> Listagem de Clientes</h2>
                  por ordem alfabetica.
                  <table border="1">
                    <tr>
                        <th align="center"> # </th>
                        <th align="center"> nº cliente </th>
                        <th align="center"> Nome </th>
                        <th align="center"> Apelido </th>
                        <th align="center"> m/f </th>
                        <th align="center"> nascido a </th>
                        <th align="center"> foto </th>
                        <th align="center"> ID </th>
                        <th align="center"> ºn </th>
                        <th align="center"> comentario </th>
                        
                                                
                    </tr>  
                    <xsl:for-each select="/db/clientes/cliente" >
                        <xsl:sort select="firstname"/>
                        <xsl:sort select="lastname"/>
                        <tr>
                            <td align="center"> <xsl:value-of select="position()"/> </td>
                            <td align="center"> <xsl:value-of select="@idcliente"/> </td>
                            <td align="center"> <xsl:value-of select="firstname"/> </td>
                            <td align="center"> <xsl:value-of select="lastname"/> </td>
                            <td align="center">
                                <xsl:if test="./@sex"> <xsl:value-of select="@sex"/> </xsl:if>
                            </td>
                            
                            
                            <td align="center"> <xsl:value-of select="nascimento"/> </td>
                            
                            <td align="center">
                                <xsl:variable name="v1" select="foto"/>
                                <img src="{$v1}" alt="X" title="{$v1}" width="30" height="30"/>
                            </td>
                            <td align="center">
                                <xsl:if test="./cartao/cc"> cc </xsl:if>
                                <xsl:if test="./cartao/bi"> bi </xsl:if>
                            </td>
                            <td align="center">
                                <xsl:if test="./cartao/cc">  <xsl:value-of select="cartao/cc"/>  </xsl:if>
                                <xsl:if test="./cartao/bi">  <xsl:value-of select="cartao/bi"/>  </xsl:if>
                            </td>
                            <td align="center">
                                <xsl:if test="./comentario"> 
                                    <xsl:value-of select="comentario"/> 
                                </xsl:if> 
                            </td>
                            
                        </tr>
                    </xsl:for-each>
                  </table>
                  <br/>
                  
                  <!-- ================================================= -->
                  
                  <h2> Listagem de Alugeres</h2>
                  por data.
                  
                  <table border ="2">
                    <tr>
                        <th align="center"> # </th>
                        <th align="center"> dia </th>
                        <th align="center"> Cliente </th>
                        <th align="center"> Carro </th>
                        <th align="center"> Duracao </th>                        
                        <th align="center"> Preço </th>
                        <th align="center"> Preço dia </th>
                        <th align="center"> Comentario </th>
                        <th align="center"> Pagamentos </th>
                                                
                    </tr>  
                    <xsl:for-each select="/db/alugeres/aluger" >
                        <xsl:sort select="inicio"/>
                        
                        <tr>
                            <td align="center"> <xsl:value-of select="position()"/> </td>
                            <td align="center"> <xsl:value-of select="inicio"/> </td>
                            <td align="center"> <xsl:value-of select="idcliente"/> </td>
                            <td align="center"> <xsl:value-of select="idcarro"/> </td>
                            <td align="center"> <xsl:value-of select="duracao"/> </td>
                            <td align="center"> <xsl:value-of select="preco"/> € </td>
                            <!-- preco dia = preco / numero dias. , formatamos o numero para 2 casas decimais. format-number($pi,'#.####')-->
                            <td align="center"> <xsl:value-of select="format-number(./preco div ./duracao,'#.##')"/> € </td>
                            <td align="center">
                                <xsl:if test="./comentario"> <xsl:value-of select="comentario"/> </xsl:if> 
                                <xsl:if test="not(./comentario)"> - -- - </xsl:if> 
                            </td>
                            <td align="center">
                                <ul>
                                    <xsl:for-each select="./pagamento" >
                                        <li>
                                            Pagou <xsl:value-of select="@euros"/>&#160;<!--espaco--><xsl:value-of select="@moeda"/> em 
                                            <xsl:value-of select="@datapagamento"/> por
                                            <xsl:value-of select="@tipopagamento"/> .
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </td>
                        </tr>
                        
                        
                    </xsl:for-each>
                    
                    
                  </table>
                  
                  
                  
                  <!--
                  <ol>
                  <xsl:for-each select="/db/alugeres/aluger" >
                      <xsl:sort select="inicio"/>
                      
                      <li>
                          <xsl:value-of select="inicio"/> 
                      </li>
                      Cliente: <xsl:value-of select="idcliente"/> 
                      Carro:   <xsl:value-of select="idcarro"/> 
                      
                  </xsl:for-each>
                  </ol>
                  -->
         </body>
        </html>  
    </xsl:template>
    
    
     
    
</xsl:stylesheet>
