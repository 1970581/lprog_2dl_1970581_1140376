<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : transformxml.xsl
    Created on : May 12, 2017, 6:20 PM
    Author     : Hugo
    Description:
        Purpose of transformation follows.
        Transformas os dados, exportando em cada aluger a info do veiculo e do cliente.
        Origina dados duplicados...
        
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" encoding="UTF-8"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    
    
    <xsl:template match="/">
        <db>
            <veiculos>            </veiculos>
            <clientes>            </clientes>
            <alugeres>
                        <xsl:apply-templates select="db/alugeres/aluger" />
            </alugeres>
        </db>
    </xsl:template>    
    
    
    <xsl:template match="aluger"> 
        <xsl:variable name="vidaluger" select="@idaluger"/>
        <xsl:variable name="vidcliente" select="idcliente"/>
        <xsl:variable name="vidcarro" select="idcarro"/>
            
        <aluger idaluger="{$vidaluger}">
            
            <inicio> <xsl:value-of select="inicio"/> </inicio>
                <duracao> <xsl:value-of select="duracao"/> </duracao>
                <preco> <xsl:value-of select="preco"/> </preco>
                <idcliente> <xsl:value-of select="idcliente"/> </idcliente>
                <idcarro> <xsl:value-of select="idcarro"/> </idcarro>
                <xsl:if test="comentario">
                    <comentario> <xsl:value-of select="comentario"/> </comentario>
                </xsl:if>
                <!-- PAGAMENTOS-->  <!-- o copy-of copia todo o elemento (. indica o elemento selecionado, pleo for each) -->
                <xsl:apply-templates select="pagamento" />
                
                <xsl:apply-templates select="/db/veiculos/veiculo[@matricula = $vidcarro]" />
                
                <xsl:apply-templates select="//cliente[@idcliente = $vidcliente]" />
        </aluger>
    </xsl:template>
    
    <xsl:template match="pagamento"> 
        <xsl:copy-of select="."/>
    </xsl:template>
    
    
    <xsl:template match="veiculo"> 
        <xsl:copy-of select="."/>        
    </xsl:template>
    
    <xsl:template match="cliente"> 
        <xsl:copy-of select="."/>        
    </xsl:template>
        
    
    
    
    <!--<xsl:template match="/">   ANTIGO NAO FAZER MATCH NAO USADO, versao antiga antes de passar para Templates.-->
    <xsl:template match="/db" mode="donotmatch">
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<!--<db xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="t1Schema.xsd">-->
<db>
    <veiculos>
    </veiculos>
    <clientes>
    </clientes>
    <alugeres>
        <xsl:for-each select="/db/alugeres/aluger" >
            
            <!-- VARIAVEIS -->
            <xsl:variable name="vidaluger" select="@idaluger"/>
            <xsl:variable name="vidcliente" select="idcliente"/>
            <xsl:variable name="vidcarro" select="idcarro"/>
            
            <!-- ALUGER -->
            <aluger idaluger="{$vidaluger}">
                
                <inicio> <xsl:value-of select="inicio"/> </inicio>
                <duracao> <xsl:value-of select="duracao"/> </duracao>
                <preco> <xsl:value-of select="preco"/> </preco>
                <idcliente> <xsl:value-of select="idcliente"/> </idcliente>
                <idcarro> <xsl:value-of select="idcarro"/> </idcarro>
                <xsl:if test="comentario">
                    <comentario> <xsl:value-of select="comentario"/> </comentario>
                </xsl:if>
                <!-- PAGAMENTOS-->  <!-- o copy-of copia todo o elemento (. indica o elemento selecionado, pleo for each) -->
                <xsl:for-each select="pagamento">
                    <xsl:copy-of select="."/>
                </xsl:for-each>      
                
                
                
                <!-- veiculo -->
                    <xsl:copy-of select="/db/veiculos/veiculo[@matricula = $vidcarro]"/>
                <!-- se soube-se do xsl:copy antes.... por agora fica aqui o codigo deste comentado os outros, usamos a mao visto ser intercente o xsl:choose -->
                <!-- 
                <veiculo matricula="{$vidcarro}">
                    <tipo> <xsl:value-of select="/db/veiculos/veiculo[@matricula = $vidcarro]/tipo"/></tipo>
                    <combustivel><xsl:value-of select="/db/veiculos/veiculo[@matricula = $vidcarro]/combustivel"/></combustivel>
                    <capacidade> <xsl:value-of select="/db/veiculos/veiculo[@matricula = $vidcarro]/capacidade"/> </capacidade>
                    <xsl:if test="/db/veiculos/veiculo[@matricula = $vidcarro]/comentario">
                        <comentario> <xsl:value-of select="/db/veiculos/veiculo[@matricula = $vidcarro]/comentario"/> </comentario>
                    </xsl:if>
                </veiculo>
                -->
                
                <!-- CLIENTE --> <!--Duplicado por causa do atributo que pode ou nao ter...-->
                <xsl:choose>
                    <xsl:when test="/db/clientes/cliente[@idcliente = $vidcliente and not(@sex)]">
                        
                            <cliente idcliente="{$vidcliente}">
                                <firstname> <xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/firstname"/> </firstname>    
                                <lastname> <xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/lastname"/>  </lastname>    
                                <cartao>
                                    <xsl:if test="/db/clientes/cliente[@idcliente = $vidcliente]/cartao[cc]">
                                        <cc><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/cartao/cc"/></cc>
                                    </xsl:if>
                                    <xsl:if test="/db/clientes/cliente[@idcliente = $vidcliente]/cartao[bi]">
                                        <bi><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/cartao/bi"/></bi>
                                    </xsl:if>
                                </cartao>
                                    <carta><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/carta"/></carta>
                                    <nascimento><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/nascimento"/></nascimento>
                                    <foto><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/foto"/></foto>
                                    <xsl:if test="/db/clientes/cliente[@idcliente = $vidcliente and comentario]">
                                        <comentario><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/comentario"/></comentario>
                                    </xsl:if>
                            </cliente>
                            
                    </xsl:when>
                    <xsl:otherwise>
                        
                            <xsl:variable name="vsex" select="/db/clientes/cliente[@idcliente = $vidcliente]/@sex"/>
                            <cliente idcliente="{$vidcliente}" sex="{$vsex}">
                                <firstname> <xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/firstname"/> </firstname>    
                                <lastname> <xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/lastname"/>  </lastname>    
                                <cartao>
                                    <xsl:if test="/db/clientes/cliente[@idcliente = $vidcliente]/cartao[cc]">
                                        <cc><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/cartao/cc"/></cc>
                                    </xsl:if>
                                    <xsl:if test="/db/clientes/cliente[@idcliente = $vidcliente]/cartao[bi]">
                                        <bi><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/cartao/bi"/></bi>
                                    </xsl:if>
                                </cartao>
                                    <carta><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/carta"/></carta>
                                    <nascimento><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/nascimento"/></nascimento>
                                    <foto><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/foto"/></foto>
                                    <xsl:if test="/db/clientes/cliente[@idcliente = $vidcliente and comentario]">
                                        <comentario><xsl:value-of select="/db/clientes/cliente[@idcliente = $vidcliente]/comentario"/></comentario>
                                    </xsl:if>
                            </cliente>
                                
                    </xsl:otherwise>
                    </xsl:choose>
                <!-- CLIENTE -->
                
            </aluger>
        </xsl:for-each>
    </alugeres>
</db>        
    </xsl:template>

</xsl:stylesheet>
