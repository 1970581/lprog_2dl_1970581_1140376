<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : parcial2.xsl
    Created on : May 12, 2017, 4:59 PM
    Author     : Hugo
    Description:
        Purpose of transformation follows.
        Obter informacao de veiculos.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    
    <xsl:template match="/db">
        <html>
            <head>
                <title>parcial2.xsl</title>
            </head>
            <body>
                <xsl:apply-templates select="veiculos" />
            </body>
        </html>
    </xsl:template>
    
    
    <xsl:template match="veiculos"> 
        <h2> Veiculos info</h2>
                Tabela 1 - Alugeres por carro, lucro e genero do cliente.<br/>
                <table border ="2">
                    <tr>
                        <th align="center"> Matricula </th>
                        <th align="center"> nº lugares </th>
                        <th align="center"> nº alugeres </th>
                        <th align="center"> lucro </th>
                        <th align="center"> Masculinos </th>
                        <th align="center"> Femeninos </th>
                        <th align="center"> Não respondeu </th>
                    </tr>
                    <xsl:apply-templates select="veiculo" />
                </table>
                <br/>
    </xsl:template>
    
    <xsl:template match="veiculo">    
        <xsl:variable name="vmatricula" select="@matricula"/>
                    
                    <tr>
                        <td align="center">
                            <xsl:value-of select="@matricula"/>
                        </td>
                        <td align="center">
                            <xsl:value-of select="capacidade"/>
                        </td>
                        
                        <!-- somatorio de alugeres -->
                        <td align="center">
                            <!--<xsl:value-of select="count(/db/alugeres/aluger/idcarro[text()=$vmatricula])"/>-->
                            <xsl:value-of select="count(//aluger/idcarro[text()=$vmatricula])"/>
                        </td>
                        
                        <!-- somatorio de pagamentos dos alugers : arredonda o sumatorio de todos os preco de aluger cujo idcliente e o da variavel-->
                        <td align="center">
                            <!--<xsl:value-of select="round(sum(/db/alugeres/aluger[idcarro = $vmatricula]/preco))"/>-->
                            <xsl:value-of select="round(sum(//aluger[idcarro = $vmatricula]/preco))"/>
                            &#160;€
                        </td>
                        <!-- numero de homens que alugaram o carro -->
                        <td align="center">
                            <!--<xsl:value-of select="count(/db/alugeres/aluger[idcarro = $vmatricula and idcliente = /db/clientes/cliente[@sex = 'f']/@idcliente])"/>-->
                            <xsl:value-of select="count(//aluger[idcarro = $vmatricula and idcliente = //cliente[@sex = 'f']/@idcliente])"/>
                            <!--  NAO FUNCIONA, substituido pelo acima.
                            <xsl:for-each select="/db/clientes/cliente[@sex = 'm']">
                                <xsl:variable name="vm" select="@idcliente"/>
                                <xsl:value-of select="count(/db/alugeres/aluger[idcarro = $vmatricula and idcliente = $vm])"/>
                            </xsl:for-each>
                            -->
                        </td>
                        <td align="center">
                            
                            <!--<xsl:value-of select="count(/db/alugeres/aluger[idcarro = $vmatricula and idcliente = /db/clientes/cliente[@sex = 'm']/@idcliente])"/>-->
                            <xsl:value-of select="count(//aluger[idcarro = $vmatricula and idcliente = //cliente[@sex = 'm']/@idcliente])"/>
                            
                        </td>
                        <td align="center">
                            
                            <!--<xsl:value-of select="count(/db/alugeres/aluger[idcarro = $vmatricula and idcliente = /db/clientes/cliente[not(@sex)]/@idcliente])"/>-->
                            <xsl:value-of select="count(//aluger[idcarro = $vmatricula and idcliente = //cliente[not(@sex)]/@idcliente])"/>
                            
                        </td>
                        
                    </tr>
                    
    </xsl:template>
    
    
    <!-- Anterior a implementação de mais templates DO NOT MATCH
    <xsl:template match="/">
    -->
    <xsl:template match="DOnotMATCH">
        <html>
            <head>
                <title>parcial2.xsl</title>
            </head>
            <body>
                <h2> Veiculos info</h2>
                
                <table border ="2">
                    <tr>
                        <th align="center"> Matricula </th>
                        <th align="center"> nº lugares </th>
                        <th align="center"> nº alugeres </th>
                        <th align="center"> lucro </th>
                        <th align="center"> M </th>
                        <th align="center"> F </th>
                        <th align="center"> não respondeu </th>
                    </tr>
                    <xsl:for-each select="/db/veiculos/veiculo" >
                    <xsl:variable name="vmatricula" select="@matricula"/>
                    
                    <tr>
                        <td align="center">
                            <xsl:value-of select="@matricula"/>
                        </td>
                        <td align="center">
                            <xsl:value-of select="capacidade"/>
                        </td>
                        
                        <!-- somatorio de alugeres -->
                        <td align="center">
                            <xsl:value-of select="count(/db/alugeres/aluger/idcarro[text()=$vmatricula])"/>
                        </td>
                        
                        <!-- somatorio de pagamentos dos alugers : arredonda o sumatorio de todos os preco de aluger cujo idcliente e o da variavel-->
                        <td align="center">
                            <xsl:value-of select="round(sum(/db/alugeres/aluger[idcarro = $vmatricula]/preco))"/>
                            &#160;€
                        </td>
                        <!-- numero de homens que alugaram o carro -->
                        <td align="center">
                            <xsl:value-of select="count(/db/alugeres/aluger[idcarro = $vmatricula and idcliente = /db/clientes/cliente[@sex = 'f']/@idcliente])"/>
                            <!--  NAO FUNCIONA, substituido pelo acima.
                            <xsl:for-each select="/db/clientes/cliente[@sex = 'm']">
                                <xsl:variable name="vm" select="@idcliente"/>
                                <xsl:value-of select="count(/db/alugeres/aluger[idcarro = $vmatricula and idcliente = $vm])"/>
                            </xsl:for-each>
                            -->
                        </td>
                        <td align="center">
                            
                            <xsl:value-of select="count(/db/alugeres/aluger[idcarro = $vmatricula and idcliente = /db/clientes/cliente[@sex = 'm']/@idcliente])"/>
                            
                        </td>
                        <td align="center">
                            
                            <xsl:value-of select="count(/db/alugeres/aluger[idcarro = $vmatricula and idcliente = /db/clientes/cliente[not(@sex)]/@idcliente])"/>
                            
                        </td>
                        
                    </tr>
                    
                    </xsl:for-each>
                </table>
                M - F - ? : representam o numero de pessoas com sexo masculino, femenino ou desconhecido que alugaram este veiculo. (xpath complexo)<br/>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
