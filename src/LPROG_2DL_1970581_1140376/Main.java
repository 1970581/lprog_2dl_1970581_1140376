package LPROG_2DL_1970581_1140376;

import LPROG_2DL_1970581_1140376.obj.Aluger;
import LPROG_2DL_1970581_1140376.obj.Cliente;
import LPROG_2DL_1970581_1140376.obj.GeradorAluger;
import LPROG_2DL_1970581_1140376.obj.Pagamento;
import LPROG_2DL_1970581_1140376.obj.Printer;
import LPROG_2DL_1970581_1140376.obj.Veiculo;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hugo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Veiculo v1 = new Veiculo("25-22-XX", "carro", "gasolina", 1, null);
        System.out.println(v1);
        
        Cliente c1 = new Cliente(1, "Joao", "Rocha", null, true, "123456789 aa1", "P-1234567", "1900-01-01", "12345678.jpg", null);
        System.out.println(c1);
        
        
        
        Aluger a1 = new Aluger(1 , "2016-05-20", 5, 500.00f, 1, "XX-22-XX", null, null);
        a1.addPagamento(new Pagamento(0, "2016-05-20", 249.50f));
        a1.addPagamento(new Pagamento(1, "2016-05-21", 000.50f));
        a1.addPagamento(new Pagamento(2, "2016-05-22", 250.00f));
        System.out.println(a1);
        
        
        List<Veiculo> listaVeiculo = new ArrayList();
        List<Cliente> listaCliente = new ArrayList();
        List<Aluger> listaAluger = new ArrayList();
        
        
        listaVeiculo.add(new Veiculo("12-22-AB", "carro", "gasolina", 5, " barato "));
        listaVeiculo.add(new Veiculo("12-23-CB", "carro", "gasolina", 5, " barato "));
        listaVeiculo.add(new Veiculo("12-23-XB", "carro", "gasolina", 5, " pisca fundido "));
        listaVeiculo.add(new Veiculo("12-22-AL", "carro", "gasolina", 5, " risco na mala "));
        listaVeiculo.add(new Veiculo("12-22-SB", "carro", "gasolina", 5, " vermelho "));
        
        listaVeiculo.add(new Veiculo("12-23-MQ", "carro", "gasolina", 5, " cor feia "));
        listaVeiculo.add(new Veiculo("12-12-QF", "carro", "gasolina", 5, " modelo feio "));
        listaVeiculo.add(new Veiculo("12-62-AB", "carro", "gasolina", 5, " bonito "));
        listaVeiculo.add(new Veiculo("12-32-FB", "carro", "gasóleo", 2, " comercial "));
        listaVeiculo.add(new Veiculo("12-62-AC", "carro", "gasóleo", 2, " comercial "));
        
        listaVeiculo.add(new Veiculo("12-23-MZ", "carro", "GPL", 5, " apenas superficie "));
        listaVeiculo.add(new Veiculo("12-19-QF", "carro", "GPL", 5, " economico "));
        listaVeiculo.add(new Veiculo("AC-32-23", "carro", "GPL", 5, " economico "));
        listaVeiculo.add(new Veiculo("AC-22-45", "carrinha", "gasolina", 5, " familiar "));
        listaVeiculo.add(new Veiculo("AC-61-67", "carrinha", "GPL", 5, " economico "));
                
        listaVeiculo.add(new Veiculo("AC-63-67", "carrinha", "gasóleo", 9, " nove lugares "));
        listaVeiculo.add(new Veiculo("AC-62-63", "carrinha", "gasóleo", 3, " de carga "));
        listaVeiculo.add(new Veiculo("AC-65-17", "carrinha", "gasóleo", 7, " sujo "));
        listaVeiculo.add(new Veiculo("AC-15-11", "carrinha", "híbrido", 3, " ecologico carga"));
        listaVeiculo.add(new Veiculo("AC-45-12", "carrinha", "híbrido", 5, " ecologico familiar "));
        
        //                     idcliente, firstname, lastname, boolean cc, ccbi,   carta,  dataNascimento,      foto,         comentario) 
        listaCliente.add(new Cliente(  1, "Joao", "Rocha",   'm',   true, "100004001 aa1", "P-1000101", "1950-01-01", "12345678.jpg", " paga tarde"));
        listaCliente.add(new Cliente(  2, "Pedro", "Rolo",   'm',   true, "100004002 aa1", "P-1000102", "1961-01-01", "12345678.jpg", " mal educado"));
        listaCliente.add(new Cliente(  3, "Pedro", "Aolo",   'm',   true, "100004003 aa1", "P-1000103", "1931-01-01", "12345678.jpg", " paga horas"));
        listaCliente.add(new Cliente(  4, "Gaspar", "Rodrigo",'m',  true, "100004004 aa1", "P-1000104", "1997-01-01", "12345678.jpg", " regila"));
        listaCliente.add(new Cliente(  5, "Moita", "Flores",  null,  true, "100004005 aa1", "P-1000105", "1953-01-01", "12345678.jpg", " alto"));
        
        listaCliente.add(new Cliente(  6, "Robarto", "Fuchia", 'm', true, "100004006 aa1", "P-1000106", "1991-01-01", "12345678.jpg", " primo do papa"));
        listaCliente.add(new Cliente(  7, "Ana", "Matias",   'f',   true, "100004007 aa1", "P-1000107", "1995-01-01", "12345678.jpg", " prima da Lili"));
        listaCliente.add(new Cliente(  8, "Roberta", "Chingaki",'f',true, "100004008 aa1", "P-1000108", "1997-01-01", "12345678.jpg", " sobrinha do dono"));
        listaCliente.add(new Cliente(  9, "Carla", "Orlado", 'f',   true, "100004009 aa1", "P-1000109", "1900-01-01", "12345678.jpg", " rica"));
        listaCliente.add(new Cliente(  10, "Tania", "Topaz",  'f',  true, "100004010 aa1", "P-1000110", "1914-01-01", "12345678.jpg", " da gorjeta"));
        
        listaCliente.add(new Cliente(  11, "Marta", "Novais",   'f',    false, "10004011", "P-1000111", "1997-01-01", "12345678.jpg", " da gorjeta"));
        listaCliente.add(new Cliente(  12, "Rita", "Pitucho",   'f',    false, "10004012", "P-1000112", "1997-02-01", "12345678.jpg", " loira"));
        listaCliente.add(new Cliente(  13, "Orla", "Marinha",   'f',    false, "10004013", "P-1000113", "1997-03-01", "12345678.jpg", " morena"));
        listaCliente.add(new Cliente(  14, "Marina", "Carvalho", 'f',   false, "10004014", "P-1000114", "1997-04-01", "12345678.jpg", " tem cao"));
        listaCliente.add(new Cliente(  15, "Marta", "Sousa",    'f',    false, "10004015", "P-1000115", "1997-05-01", "12345678.jpg", " tem gato"));
        
        listaCliente.add(new Cliente(  16, "Marta", "Sousa",   'f',    false, "10004016", "P-1000116", "1999-03-01", "12345678.jpg", " gosta de miudos"));
        listaCliente.add(new Cliente(  17, "Alga", "Bousa",    'f',    false, "10004017", "P-1000117", "2004-03-01", "12345678.jpg", " gosta de pandas"));
        listaCliente.add(new Cliente(  18, "Talia", "Algalia", 'f',    false, "10004018", "P-1000118", "1996-07-01", "12345678.jpg", " nao cabe no carro"));
        listaCliente.add(new Cliente(  19, "Joana", "Tomate",  'f',     false, "10004019", "P-1000119", "1970-06-01", "12345678.jpg", " gosta de legumes"));
        listaCliente.add(new Cliente(  20, "Stalin", "Costa",   'm',    false, "10004020", "P-1000120", "1914-04-01", "12345678.jpg", " gosta de economicos"));
        
        
        GeradorAluger gerador = new GeradorAluger(listaVeiculo, listaCliente, listaAluger);
        gerador.gerarLista();
        
        new Printer(listaVeiculo, listaCliente, listaAluger).print();
    }
    
    
    
    
    
}
