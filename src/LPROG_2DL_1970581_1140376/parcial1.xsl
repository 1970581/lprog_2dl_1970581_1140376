<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : parcial1.xsl
    Created on : May 12, 2017, 2:47 PM
    Author     : Hugo
    Description:
        Purpose of transformation follows.
        Mostrar pagamentos relacionados com clientes.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    
    <xsl:template match="/db">
        <html>
            <head>
                <title>parcial1.xsl</title>
            </head>
            <body>
                            <xsl:apply-templates select="clientes" />
                            <xsl:apply-templates select="clientes" mode = "two"/>
            </body>
        </html>
    </xsl:template>            
    
    <xsl:template match="clientes">
        <h2> Pagamentos generico </h2>
                Tabela 1 - Totais de alugeres e valores cobrados por cliente.<br/>
                <table border ="2">
                    <tr>
                        <th align="center"> # </th>
                        <th align="center"> Nome </th>
                        <th align="center"> nº alugeres </th>
                        <th align="center"> Pago </th>
                    </tr>
                            <xsl:apply-templates select="cliente" />
                </table>                
                <br/>
    </xsl:template>
    
    
    <xsl:template match="cliente">
        <xsl:variable name="v0Cliente" select="@idcliente"/>
        <tr>
            <td align="center">
                <xsl:value-of select="@idcliente"/>
            </td>
            <td align="center">
                <xsl:value-of select="firstname"/>&#160;
                <xsl:value-of select="lastname"/>
            </td>
                        
            <!-- somatorio de alugeres -->
            <td align="center">
                <!--
                    <xsl:value-of select="count(/db/alugeres/aluger/idcliente[text()=$v0Cliente])"/>
                -->
                <xsl:value-of select="count(//aluger/idcliente[text()=$v0Cliente])"/>
            </td>
                        
            <!-- somatorio de pagamentos dos alugers : arredonda o sumatorio de todos os preco de aluger cujo idcliente e o da variavel-->
            <td align="center">
                <!-- substituido por + generico
                    <xsl:value-of select="round(sum(/db/alugeres/aluger[idcliente = $v0Cliente]/preco))"/>
                -->
                <xsl:value-of select="round(sum(//aluger[idcliente = $v0Cliente]/preco))"/>
                            &#160;€
            </td>
        </tr>
        <!--<xsl:value-of select="@datapagamento"/>-->
        
        
        
    </xsl:template>
    
    
    <xsl:template match="clientes" mode="two">
        
        <h2> Listagem de pagamentos por cliente e aluger</h2>
        
        <xsl:apply-templates select="cliente" mode="two" />
    </xsl:template>
    
    <xsl:template match="cliente" mode="two">
        <xsl:variable name="vCliente" select="@idcliente"/>
                    <strong>
                    Cliente:&#160;[<xsl:value-of select="@idcliente"/>]&#160;<xsl:value-of select="firstname"/>&#160;<!--espaco--><xsl:value-of select="lastname"/>
                    </strong>
                    <br/>
                    
                    <!--<xsl:for-each select="/db/alugeres/aluger" >-->
                    <xsl:for-each select="//aluger" >
                        <xsl:if test="idcliente = $vCliente"> 
                            <p>
                                &#160;&#160;<!--espaço-->
                                Pagamento esperado:&#160;<xsl:value-of select="preco"/>&#160;€&#160;do cliente com nº cliente:&#160; <xsl:value-of select="idcliente"/><br/>
                                <ul>
                                    <xsl:for-each select="./pagamento" >
                                        <li>
                                            &#160;&#160;&#160;+&#160;<!--espaço-->
                                            <xsl:value-of select="@euros"/>&#160;<xsl:value-of select="@moeda"/>&#160;
                                            a&#160;<xsl:value-of select="@datapagamento"/>&#160;(<xsl:value-of select="@tipopagamento"/>)
                                        </li>
                                    </xsl:for-each>
                                </ul>
                                &#160;&#160;<!--espaço-->
                                Total pago: &#160; <xsl:value-of select="sum(./pagamento/@euros)"/> &#160; €<br/>
                            </p>
                        </xsl:if>
                    </xsl:for-each>   
                    <br/>    
    </xsl:template>
    
    
    
    
    
    
    
    <!-- ============================================================================================== -->
    
    
    
    
<!--
    <xsl:template match="/db">
-->
    <xsl:template match="/db3333">
        <html>
            <head>
                <title>parcial1.xsl</title>
            </head>
            <body>
                
                <h2> Pagamentos generico</h2>
                
                <table border ="2">
                    <tr>
                        <th align="center"> # </th>
                        <th align="center"> Nome </th>
                        <th align="center"> nº alugeres </th>
                        <th align="center"> Pago </th>
                    </tr>
                
                
                <xsl:for-each select="/db/clientes/cliente" >
                    <xsl:variable name="v0Cliente" select="@idcliente"/>
                    <tr>
                        <td align="center">
                            <xsl:value-of select="@idcliente"/>
                        </td>
                        <td align="center">
                            <xsl:value-of select="firstname"/>&#160;<xsl:value-of select="lastname"/>
                        </td>
                        
                        <!-- somatorio de alugeres -->
                        <td align="center">
                            <xsl:value-of select="count(/db/alugeres/aluger/idcliente[text()=$v0Cliente])"/>
                        </td>
                        
                        <!-- somatorio de pagamentos dos alugers : arredonda o sumatorio de todos os preco de aluger cujo idcliente e o da variavel-->
                        <td align="center">
                            <xsl:value-of select="round(sum(/db/alugeres/aluger[idcliente = $v0Cliente]/preco))"/>
                            &#160;€
                        </td>
                        
                        
                    </tr>
                    <xsl:value-of select="@datapagamento"/>
                    
                    
                </xsl:for-each>
                
                </table>
                
                
                
                
                
                
                
                
                <!-- ================================================== -->
                
                
                <h2> Pagamentos discriminados</h2>
                <xsl:for-each select="/db/clientes/cliente" >
                    <xsl:variable name="vCliente" select="@idcliente"/>
                    <strong>
                    Cliente:&#160;<xsl:value-of select="@idcliente"/>&#160;<xsl:value-of select="firstname"/>&#160;<!--espaco--><xsl:value-of select="lastname"/>
                    </strong>
                    <br/>
                    
                    <xsl:for-each select="/db/alugeres/aluger" >
                        <xsl:if test="idcliente = $vCliente"> 
                            <p>
                                &#160;&#160;<!--espaço-->
                                Pagamento esperado:<xsl:value-of select="preco"/>&#160;€&#160;do cliente nº cliente:&#160; <xsl:value-of select="idcliente"/><br/>
                                <ul>
                                    <xsl:for-each select="./pagamento" >
                                        <li>
                                            &#160;&#160;&#160;+&#160;<!--espaço-->
                                            <xsl:value-of select="@euros"/>&#160;<xsl:value-of select="@moeda"/>&#160;
                                            a&#160;<xsl:value-of select="@datapagamento"/>&#160;(<xsl:value-of select="@tipopagamento"/>)
                                        </li>
                                    </xsl:for-each>
                                </ul>
                                &#160;&#160;<!--espaço-->
                                Total:<xsl:value-of select="sum(./pagamento/@euros)"/>€<br/>
                            </p>
                        </xsl:if>
                    </xsl:for-each>   
                    <br/>    
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
