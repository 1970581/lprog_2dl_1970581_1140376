<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : relatorioHTML.xsl
    Created on : May 27, 2017, 6:02 PM
    Author     : Hugo
    Description:
        Purpose of transformation follows.
-->

<!--<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">-->
<!--<xsl:stylesheet xmlns:t="http://www.dei.isep.ipp.pt/lprog" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">-->
<!--<xsl:stylesheet xmlns="http://www.dei.isep.ipp.pt/lprog" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">-->
<!--<xsl:stylesheet version="2.0" xpath-default-namespace="http://www.dei.isep.ipp.pt/lprog" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >-->

<xsl:stylesheet xmlns:r="http://www.dei.isep.ipp.pt/lprog" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
        
    <xsl:output method="html"/>
    <!--<xsl:output method="html" encoding="iso-8859-1"/>-->
    <!--<xsl:output method="xml" encoding="UTF-8"/>-->
    <!--<xsl:output method="xml" encoding="iso-8859-1"/>-->
    

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <!--
    <xsl:template match="/" >
        <xsl:copy-of select="."/>
    </xsl:template>
    -->
    
    
    <xsl:template match="/" >
        <html>
            <body>
                <!--<xsl:apply-templates select="r:relatório" />-->
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>
    

    <xsl:template match="r:relatório">   
        <xsl:text>relatório/@id: </xsl:text>
            <xsl:value-of select ="@id"/> 
            <br/>
        <h1> Relatório </h1>
         <h2> ============================================================ </h2>
        <xsl:apply-templates />
        <!--<xsl:apply-templates />-->
        <!--<xsl:apply-templates select="child::" />-->
    </xsl:template>
    


    <xsl:template match="r:páginaRosto">
        <h1> Tema: <xsl:value-of select="r:tema"/> </h1>
        
        <!-- imagem -->
        <xsl:apply-templates select="r:logotipoDEI"/>
        
        <strong> <xsl:text>Disciplina: </xsl:text>        </strong>
        <xsl:value-of select ="./r:disciplina/r:designação"/> 
        <xsl:text> &#160;[</xsl:text>
        <xsl:value-of select ="./r:disciplina/r:sigla"/> 
        <xsl:text>]&#160;Ano </xsl:text>
        <xsl:value-of select ="./r:disciplina/r:anoCurricular"/> 
        <xsl:text> &#160; Turma: </xsl:text>
        <xsl:value-of select ="./r:turma"/> <br/> 
        <strong> <xsl:text>Autores: </xsl:text>        </strong> 
        <ul> 
            <xsl:for-each select="r:autor" >    
                <li>
                    <xsl:apply-templates select="."/>
                </li>
            </xsl:for-each> 
        </ul>
        <strong> <xsl:text>Professores: </xsl:text>        </strong> 
        <ul> 
            <xsl:for-each select="r:professor" >    
                <li>
                    <xsl:apply-templates select="."/>
                </li>
            </xsl:for-each> 
        </ul>
        <xsl:apply-templates select="r:data"/>
        <!--
        <xsl:apply-templates select="r:autor"/>
        <xsl:apply-templates />
        -->
        <hr/>
        <h2> Indice </h2>
        <ul>
        <xsl:for-each select="//*[@id]" >
            <!-- and ( //name()='subsecção' or ./@tituloSecção -->
            <xsl:variable name="vref" select="@id"/>
            <xsl:if test="./@tituloSecção">
                <li>        
                    <a href="#{$vref}"><xsl:value-of select ="./@id"/></a>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select ="./@tituloSecção"/>
                </li>
            </xsl:if>
            <xsl:if test="name()='subsecção'">
                <li>
                    <a href="#{$vref}"><xsl:value-of select ="./@id"/></a>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select ="."/>
                </li>
            </xsl:if>
        </xsl:for-each>
        </ul>
        <hr/>
    </xsl:template>

    <xsl:template match="r:tema"/> <!-- vazio -->
    
    <xsl:template match="r:disciplina"> <p/>
        <xsl:text>Disciplina: </xsl:text> <xsl:value-of select ="r:designação"/> <br/>
        <xsl:text>Ano: </xsl:text> <xsl:value-of select ="r:anoCurricular"/> <br/>
        <xsl:text>Sigla: </xsl:text> <xsl:value-of select ="r:sigla"/> <br/>
    </xsl:template>

    <xsl:template match="r:autor"> 
        <xsl:text>Nome: </xsl:text> <xsl:value-of select ="r:nome"/> 
        <xsl:text> nº </xsl:text> <xsl:value-of select ="r:número"/> 
        <xsl:text> Email: </xsl:text> <xsl:value-of select ="r:mail"/> 
        <!--<br/>-->
    </xsl:template>

    <xsl:template match="r:turma">
        <xsl:text>Turma:  </xsl:text> 
        <xsl:value-of select ="."/> <br/>
    </xsl:template>
    
    <xsl:template match="r:professor">
        <strong> <xsl:value-of select="."/> </strong>
        <xsl:text> [</xsl:text> 
        <xsl:value-of select ="@tipoAula"/> 
        <xsl:text>] </xsl:text>
        <xsl:text> </xsl:text>
        <xsl:value-of select ="@sigla" /> 
        <xsl:text>@isep.ipp.pt </xsl:text> <br/>
    </xsl:template>

    <xsl:template match="r:logotipoDEI">
        <xsl:variable name="vfoto" select="."/>
        <img src="{$vfoto}" alt="X" title="{$vfoto}" width="300" height="300"/> <br/>
    </xsl:template>
    
    <xsl:template match="r:data">
        <xsl:text>Realizado a </xsl:text> 
        <xsl:value-of select="."/>  <br/>
    </xsl:template>




<!-- ================== CORPO ==================== -->


    <xsl:template match="r:corpo">
        <p/>
        <xsl:text>corpo/@id: </xsl:text> 
        <xsl:value-of select="./@id"/>  <br/>
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="r:introdução|r:análise|r:linguagem|r:transformações|r:conclusão|r:anexos">
        <h2> 
            <xsl:value-of select="./@id"/>
            <xsl:variable name="vRef" select="./@id"/> 
            <a name="{$vRef}"></a> 
            <xsl:text>: </xsl:text>
            <xsl:value-of select="./@tituloSecção"/> 
        </h2>
        <xsl:apply-templates />
        <hr/>
    </xsl:template>

    <xsl:template match="r:outrasSecções">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="r:subsecção">
        <h3>
            <xsl:variable name="vRef" select="./@id"/> 
            <a name="{$vRef}"></a> 
            <xsl:value-of select="./@id"/> <xsl:text>: </xsl:text>
            <xsl:value-of select="."/> 
        </h3>
        <!--<xsl:apply-templates />-->
        <p/>
    </xsl:template>
    
    <xsl:template match="r:parágrafo">
        <xsl:apply-templates />
        <p/>
    </xsl:template>
    
    <!-- SUB CODIGO MIXED DOS PARAGRAFOS -->
    
    <xsl:template match="r:citação">
        <xsl:variable name="vref" select="."/>        
        <a href="#{$vref}"><xsl:value-of select="."/></a>
    </xsl:template>
    
    <xsl:template match="r:negrito"> 
        <strong> <xsl:value-of select="."/> </strong> 
    </xsl:template>
    <!-- strong e not b por causa de compatibilidade-->
    
    <xsl:template match="r:itálico">
        <i> <xsl:value-of select="."/> </i>
    </xsl:template>
    
    <xsl:template match="r:sublinhado">
        <ins> <xsl:value-of select="."/> </ins>
    </xsl:template>
    <!-- ins e nao u por causa de compatibilidade-->
    
    
    <xsl:template match="r:figura">
        <xsl:variable name="vFoto" select="./@src"/> 
        <img src="{$vFoto}" alt="{$vFoto} missing" title="{./@descrição}"/>
        <br/>
        <i> <xsl:value-of select="./@descrição"/> </i>
        <br/>
    </xsl:template>
    
    <xsl:template match="r:listaItems">
        <dl>
            <xsl:apply-templates />
        </dl>
    </xsl:template>
    
    <xsl:template match="r:item">
        <xsl:if test="@label">
            <dt><strong><xsl:value-of select="@label"/></strong></dt>
        </xsl:if>
        <dd><xsl:value-of select="."/></dd>
    </xsl:template>
    
    <xsl:template match="r:codigo">
        <textarea><xsl:apply-templates /></textarea>
        <br/><xsl:text> Pf expanda a janela. </xsl:text>
    </xsl:template>
    
    <xsl:template match="r:bloco">
        
        <xsl:copy-of select="."/>
        
        
        <!--
            <xsl:text> Pf expanda a janela. </xsl:text>
            <p><xsl:copy-of select="."/></p>
            <p><xsl:copy-of select="parse(.)"/></p>
        -->
    </xsl:template>
    
    
    
    
    
    
    
    
    
    <!-- REFERENCIAS ===========================================-->
    
    <xsl:template match="r:referências">
        <h2> 
            <xsl:value-of select="./@id"/> <xsl:text>: </xsl:text>
            <xsl:value-of select="./@tituloSecção"/> 
        </h2>
        <xsl:apply-templates />
        <hr/>
    </xsl:template>
    
    <xsl:template match="r:refBibliográfica">
        <strong><xsl:text>Referencia Bibliografica: </xsl:text> </strong>
        <xsl:value-of select="@idRef"/> 
        <xsl:variable name="vRef" select="@idRef"/> 
        <a name="{$vRef}"></a>
        <br/>
        <!--  porque alguem se lembrou de colocar: 
        <xs:sequence maxOccurs="unbounded">
             <xs:element name="título" type="xs:string"/>
            <xs:element name="dataPublicação" type="xs:string"/>
            <xs:element name="autor" type="xs:string" maxOccurs="unbounded"/>
        </xs:sequence>
        -->
            <xsl:for-each select="*" >  
                <xsl:if test="name() ='título'">
                    <xsl:text> - </xsl:text> 
                    <xsl:text>Titulo: </xsl:text> 
                    <xsl:value-of select="."/> 
                </xsl:if>
                <xsl:if test="name() ='dataPublicação'">
                    <xsl:text> data: </xsl:text> 
                    <xsl:value-of select="."/> <br/>
                </xsl:if>
                <xsl:if test="name() ='autor'">
                    <xsl:text>&#160;&#160;&#160;&#160; Autor: </xsl:text> 
                    <xsl:value-of select="."/> <br/>
                </xsl:if>
                
            </xsl:for-each>
        <p/>
    </xsl:template>
    
    <xsl:template match="r:refWeb">
        <strong><xsl:text>Referencia WEB: </xsl:text> </strong>
        <xsl:value-of select="@idRef"/>
        <xsl:variable name="vRef" select="@idRef"/> 
        <a name="{$vRef}"></a> 
        <br/>
        <!--
        <xs:sequence maxOccurs="unbounded">
            <xs:element name="URL" type="xs:anyURI"/>
            <xs:element name="descrição" type="xs:string"/>
            <xs:element name="consultadoEm" type="xs:date"/>
        </xs:sequence>
        -->
         <xsl:for-each select="*" >  
                <xsl:if test="name() ='URL'">
                    <xsl:text> - </xsl:text> 
                    <xsl:text> Link: </xsl:text> 
                        <xsl:variable name="vlink" select="."/>
                        <a href="{$vlink}">
                            <xsl:value-of select="."/> 
                        </a>
                        <br/>
                </xsl:if>
                <xsl:if test="name() ='descrição'">
                    <xsl:text> &#160;&#160;&#160;&#160; Descricao: </xsl:text> 
                    <xsl:value-of select="."/> <br/>
                </xsl:if>
                <xsl:if test="name() ='consultadoEm'">
                    <xsl:text>&#160;&#160;&#160;&#160; Consultado a: </xsl:text> 
                    <xsl:value-of select="."/> <br/>
                </xsl:if>
                
            </xsl:for-each>
            <p></p>
    </xsl:template>
    
    
    <!--  and not(string(.)) -->
    
    
    
    
    
    
</xsl:stylesheet>
