<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : transformtotxt.xsl
    Created on : May 16, 2017, 3:29 PM
    Author     : Hugo
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" encoding="iso-8859-1"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/db">
        <xsl:apply-templates select="veiculos"/>
        <xsl:apply-templates select="clientes"/>
        <xsl:apply-templates select="alugeres"/>
   </xsl:template>
    
   <xsl:template match = "veiculos">
       <xsl:text>Matricula;Tipo;Combustivel;Capacidade;Comentario(Opcional)&#xa;</xsl:text>
       <xsl:text>&#xa;</xsl:text>
       <xsl:apply-templates select="veiculo"/>
       <xsl:text>&#xa;</xsl:text>
       <xsl:text>&#xa;</xsl:text>
   </xsl:template>
   
   <xsl:template match = "clientes">
       <xsl:text>idCliente; Nome; Cartao(Cc/Bi); Carta; Nascimento; Foto; Comentario(Opcional)&#xa;</xsl:text>
       <xsl:text>&#xa;</xsl:text>
       <xsl:apply-templates select="cliente"/>
       <xsl:text>&#xa;</xsl:text>
       <xsl:text>&#xa;</xsl:text>
   </xsl:template>
   
    <xsl:template match = "alugeres">
       <xsl:text>IdAluger; InicioAluger; Duração; Preço; IdCliente; IdCarro; Comentario(Opcional)&#xa;</xsl:text>
       <xsl:text>&#xa;</xsl:text>
       <xsl:apply-templates select="aluger"/>
       <xsl:text>&#xa;</xsl:text>
       <xsl:text>&#xa;</xsl:text>
   </xsl:template> 
    
    <xsl:template match="veiculo">
        <xsl:variable name="v" select="';'"/>
        
        <xsl:value-of select = "@matricula"/><xsl:value-of select="$v"/>
        <xsl:value-of select = "tipo"/><xsl:value-of select="$v"/>
        <xsl:value-of select = "combustivel"/><xsl:value-of select="$v"/>
        <xsl:value-of select = "capacidade"/><xsl:value-of select="$v"/>
        <xsl:if test="comentario">
                <xsl:value-of select="comentario"/>
        </xsl:if><xsl:value-of select="$v"/>
                <xsl:text>&#xa;</xsl:text>
    </xsl:template>
<!-- &#160; -->
    <xsl:template match="cliente">
        <xsl:variable name="v" select="';'"/>
        
        <xsl:value-of select = "@idcliente"/><xsl:value-of select="$v"/>
        <xsl:copy-of select="firstname "/> 
        <xsl:text>&#160;</xsl:text>
        <xsl:copy-of select=" lastname"/>
        <xsl:value-of select="$v"/>
        <xsl:if test="cartao/cc">
            <xsl:text>Cc</xsl:text>
            <xsl:text>&#160;</xsl:text>
            <xsl:value-of select="cartao/cc"/>
        </xsl:if>
        <xsl:if test="cartao/bi">
            <xsl:text>Bi</xsl:text>
            <xsl:text>&#160;</xsl:text>
            <xsl:value-of select="cartao/bi"/>
        </xsl:if> 
        <xsl:value-of select="$v"/>
        <xsl:copy-of select="carta"/> 
        <xsl:value-of select="$v"/>
        <xsl:copy-of select="nascimento"/> 
        <xsl:value-of select="$v"/>
        <xsl:copy-of select="foto"/>
        <xsl:value-of select="$v"/>
        <xsl:if test="comentario">
                <xsl:value-of select="comentario"/>
        </xsl:if><xsl:value-of select="$v"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    
    <xsl:template match="aluger">
        <xsl:variable name="v" select="';'"/>
        
        <xsl:value-of select = "@idaluger"/><xsl:value-of select="$v"/>
        <xsl:value-of select = "inicio"/><xsl:value-of select="$v"/>
        <xsl:value-of select = "duracao"/><xsl:value-of select="$v"/>
        <xsl:value-of select = "idcliente"/><xsl:value-of select="$v"/>
        <xsl:value-of select = "idcarro"/><xsl:value-of select="$v"/>
        <xsl:if test="comentario">
                <xsl:value-of select="comentario"/>
        </xsl:if><xsl:value-of select="$v"/>
        <xsl:apply-templates select="pagamento"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template> 
    
    <xsl:template match="pagamento">
         <xsl:variable name="v" select="';'"/>
         <xsl:value-of select = "@tipopagamento"/><xsl:value-of select="$v"/>
         <xsl:value-of select = "@datapagamento"/><xsl:value-of select="$v"/>
         <xsl:value-of select = "@euro"/><xsl:value-of select="$v"/>
         <xsl:text>Euro</xsl:text>
         <xsl:value-of select="$v"/>
         <!--<xsl:value-of select = "@moeda"/><xsl:value-of select="$v"/>-->
    </xsl:template>
</xsl:stylesheet>
